//var, let, const
let score;
score = 10;
console.log(score);


//String, Numbers, decimal, Boolean, null, undefined
const name = "John";
const age = 30;
const rating = 4.5;
const isCool = true;
const x = null;
const y = undefined;
let z;

// Concatenation
console.log(typeof rating);
console.log('My name is ' + name +'and I am '+ age);
// Template String
const hello = `My name is ${name} and I am ${age}`;

console.log(hello);

// console.log('Hello World');
// console.error('This is an error');
// console.warn('This is a warning');

const s = 'Hello World';
const x = 'technologi, computers, it, code';

console.log(s.length());
console.log(s.toUpperCase());
console.log(s.toLowerCase());
console.log(s.substring(0, 5));
console.log(x.split(', '));

//Arrays - variables that hold multiple values
const numbers = new Array('1,2,3,4,5');
const fruits = ['apples', 'oranges', 'pears'];
fruits[3] = 'grapes';
fruits.push('mangos');
fruits.unshift('strawberries');
fruits.pop();
console.log(Array.isArray('hello'));
console.log(fruits.indexOf('oranges'));
console.log(numbers);
console.log(fruits);

//#######//
const person = {
    firstName: 'Jhon',
    lastName: 'Doe',
    age: 30,
    hobbies: ['music', 'movies', 'sports'],
    address: {
        street: '50 main st',
        city: 'Boston',
        state: 'MA'
    }
}
person.email = 'john@gmail.com'

console.log(person.firstName, person.lastName);
console.log(person.address.city);
const {firstName, lastName: {city}} = person;
console.log(city);

const todos = [
    {
        id: 1,
        text: 'Take out trash',
        isCompleted: true
    },
    {
        id: 2,
        text: 'Meeting with boss',
        isCompleted: true
    },
    {
        id: 3,
        text: 'Dentist appt',
        isCompleted: false
    }    
];

console.log(todos[1].text);
const todoJSN = JSON.stringify(todos);
console.log(todoJSN);



// =================================================== 

//For #1
for(let i =  0; i <= 10; i++) {
    console.log(`For Loop Number: ${i}`);
}
//For #2
for(let i =  0; i <= todos.length; i++) {
    console.log(todo[i].text);
}
//For #3
for(let todo of todos){
    console.log(todo.id);
}
//forEach, map, filter #1
todos.forEach(function(todo) {
    console.log(todo.text)
});
//forEach, map #2
const todoText = todos.map(function(todo){
    return todo.text;
});
console.log(todoText);
//forEach, filter #3
const todoCompleted = todos.filter(function(todo){
    return todo.isCompleted === true;
});
console.log(todoCompleted);

//forEach, filter, map #4
const todoCompleted = todos.filter(function(todo){
    return todo.isCompleted === true;
}).map(function(todo){
    return todo.text;
});
console.log(todoCompleted);


// ======================================================
//While
let i = 0;
while(i < 10) {
    console.log(`Whle Loop Number: ${i}`);
    console.log();
    // i++;
}

//if else
const x = 4;
const y = 10;

if(x > 5 || y > 10) {
    console.log('x is 10');
}else if (x > 10) {
    console.log('x is greter than 10');
}else {
    console.log('x is less than 10');
};

if(x > 5 && y > 10) {
    console.log('x is more than 5 or y is more than 10');
};

//switch
const a = 11;
const color = x>10 ? 'red' : 'blue';
//console.log(color);
switch(color) {
    case 'red' :
        console.log('color is red');
        break;
    case 'blue' :
        console.log('color is blue');
        break;
    default:
        console.log('color is NOT red or blue');
        break;
};


//Function
function addNums(num1, num2) {
    // console.log(num1 + num2);
    return num1 + num2;
}
// addNums(5,4);
console.log(addNums(5,5));
// function else
const addNums1 = num3 => num3 + 5;
console.log(addNums1(5));

// Constructor function
function person(firstName, lastName, dob) {
    this.firstName = firstName;
    this.lastName = lastName;
    this.dob = new Date(dob);
    // this.getBirthYear = function() {
    //     return this.dob.getFullYear(); //if you use prototype, cannot use this
    }
    this.getFullName = function() {
        return `${this.firstName} ${this.lastName}`;
    }


person.prototype.getBirthYear = function(){
    return this.dob.getBirthYear();
}

//Class 
class person {
    constructor(firstName, lastName, dob) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.dob = new Date(dob);
    }
    getBirthYear(){
        return this.dob.getBirthYear();
    }
    getFullName() {
        return `${this.firstName} ${this.lastName}`;
    }

}

//Instantiate object
const person1 = new person('John', 'Doe', '4-3-2022');
console.log(person1);
console.log(person1.firstName);
console.log(person1.dob.getFullYear());
console.log(person1.dob.getFullName());
//Call prototype
console.log(person)