// ATTENTION: code from youtube //

// Menampilkan peringatan
alert('Hello World');
// Menampilkan variable pada console
console.log('Hello World');
// Menampilkan error messege
console.error('This is an error');
// Menampilkan peringatan
console.warn('This is a warning');


// VARIABLES - var, let, const
// Mendeklarasikan variavble dengan nilai 30
let age = 30;

// Memberikan nilai lain karna nilai pada variable bukan const
age = 31;


// DATA TYPES - String, Number, Boolean, null, undefined
// Mendeklarasikan variable string
const name = 'Brad';
// Mendeklarasikan variable number
const age = 37;
const rating = 3.5;
// Mendeklarasikan variable Boolean true or false
const isCool = true;
// Mendeklarasikan variable null
const x = null;
// Mendeklarasikan variable undefined
const y = undefined;
// Mendeklarasikan variable tanpa nilai
let z; // undefined

// Menampilkan check type variable
console.log(typeof z);


// STRINGS

// Concatenation
// Menampilkan output dari pemanggilan nilai variable name dan age
console.log('My name is ' + name + ' and I am ' + age);
// Template literal (better)
// Menampilkan output dari pemanggilan nilai variable name dan age dengan metode berbeda
console.log(`My name is ${name} and I am ${age}`);

// String methods & properties
//Mendeklarasikan variable const
const s = 'Hello World';
// Mendklarasikan variable tanpa nilai
let val;
// Get length
// Menampilkan length dari variable s
val = s.length;
// Change case
//Nilai string dikoncersi ke huruf kapital
val = s.toUpperCase();
// Nilai string dikoncersi ke huruf kecil
val = s.toLowerCase();
// Get sub string
// Mengambil bagian tertentu dari sebuah string
val = s.substring(0, 5);
// Split into array
// Membagi string menjadi array substring dan mengembalikan array baru tanpa mengubah string asli.
val = s.split('');



// ARRAYS - Store multiple values in a variable
// Mendeklarasikan nilai multiple pada variable
const numbers = [1,2,3,4,5];
const fruits = ['apples', 'oranges', 'pears', 'grapes'];
// Menampilkan nilai dari variable numbers dan fruits
console.log(numbers, fruit);

// Get one value - Arrays start at 0
// Mengambil nilai string dari array ke 1
console.log(fruits[1]);

// Add value
//Menambahkan nilai string array ke 4
fruits[4] = 'blueberries';

// Add value using push()
//Menambahkan nilai string dengan metode push dan disimpan di paling akhir
fruits.push('strawberries');

// Add to beginning
// Menambahkan nilai string dan disimpan di awal
fruits.unshift('mangos');

// Remove last value
// Menghapus nilai string array terakhir
fruits.pop();

// // Check if array
// Melakukan chec jika nilai variable adalah array
console.log(Array.isArray(fruits));

// // Get index
// Mencari posisi string pada elemen array
console.log(fruits.indexOf('oranges'));



// OBJECT LITERALS
// Mendeklarasikan const variable dengan nilai property array
const person = {
  firstName: 'John',
  age: 30,
  hobbies: ['music', 'movies', 'sports'],
  address: {
    street: '50 Main st',
    city: 'Boston',
    state: 'MA'
  }
}

// Get single value
// Memanggil object name pada variable person
console.log(person.name)

// Get array value
// Memanggil object pada property hobbies di variable person di array ke 1
console.log(person.hobbies[1]);

// Get embedded object
// Memanggil sebuah object yang disematkan pada variable person
console.log(person.address.city);

// Add property
// Menambahkan property email dengan tipe data string
person.email = 'jdoe@gmail.com';

// Array of objects
// Mendeklarasikan variable const dengan object array
const todos = [
  {
    id: 1,
    text: 'Take out trash',
    isComplete: false
  },
  {
    id: 2,
    text: 'Dinner with wife',
    isComplete: false
  },
  {
    id: 3,
    text: 'Meeting with boss',
    isComplete: true
  }
];

// Get specific object value
// Memanggil object text pada id 1
console.log(todos[1].text);

// Format as JSON
// Memanggil variable dengan format JSON
console.log(JSON.stringify(todos));


// LOOPS atau perulangan

// For
// Melakukan perulangan atau looping dengan metode for
for(let i = 0; i <= 10; i++){
  // Menampilkan hasil output looping for
  console.log(`For Loop Number: ${i}`);
}

// While
// Melakukan perulangan atau looping dengan metode While
let i = 0
while(i <= 10) {
  // Menampilkan hasil output looping While  
  console.log(`While Loop Number: ${i}`);
  i++;
}

// Loop Through Arrays
// For Loop
// Melakukan looping atau perulangan tipe array pada variable todos
for(let i = 0; i < todos.length; i++){
  console.log(` Todo ${i + 1}: ${todos[i].text}`);
}

// For...of Loop
// Melakukan looping dengan menampilkan property text pada variable
for(let todo of todos) {
  console.log(todo.text);
}


// forEach() - Loops through array
// Memanggil fungsi untuk setiap elemen dalam array
todos.forEach(function(todo, i, myTodos) {
  console.log(`${i + 1}: ${todo.text}`);
  console.log(myTodos);
});

// map() - Loop through and create new array
// Mendeklarasikan variable const dengan menggunakan fungsi map
// maps digunakan untuk membuat array
const todoTextArray = todos.map(function(todo) {
  // Mengembalikan value
  return todo.text;
});

// Menampilkan value array dari variable
console.log(todoTextArray);

// filter() - Returns array based on condition
// Mendeklarasikan variable menggunakan fungsi filter
const todo1 = todos.filter(function(todo) {
  // Return only todos where id is 1
  // Melakukan return value
  return todo.id === 1; 
});


// CONDITIONALS

// Simple If/Else Statement
// Mendeklarasikan variable const dengan value 30
const x = 30;

// Pengkondisian value 
if(x === 10) {
  //Menampilkan output
  console.log('x is 10');
  // Pengkondisian value > 10
} else if(x > 10) {
  // Menampilkan value
  console.log('x is greater than 10');
  // Pengkondisian lainnya
} else {
  // Menampilkan output
  console.log('x is less than 10')
}

// Switch
// Mendklarasikan variable
color = 'blue';

// Pengkondisian value
switch(color) {
  // Pengkondisian value
  case 'red':
    console.log('color is red'); // Menampilkan output
  // Pengkondisian value
    case 'blue':
    console.log('color is blue'); // Menampilkan output
  // Default value
    default:  
  default:  
    default:  
    console.log('color is not red or blue') // Menampilkan output
}

// Ternary operator / Shorthand if
// Mendeklarasikan variable ternary
// Ternary adalah operator yang melibatkan 3 buah operand dilambkangkan tanda ?
const z = color === 'red' ? 10 : 20;



// FUNCTIONS
// Menjalankan program greet dengan 2 parameter
function greet(greeting = 'Hello', name) {
  // Pengkondisian jika nilai name tidak sama
  if(!name) {
    // console.log(greeting);
    // Mengembalikan value 
    return greeting;
    //Pengkondisian jika nilai name dan greeting tidak sama 
  } else {
    // console.log(`${greeting} ${name}`);
    // Mengembalikan value
    return `${greeting} ${name}`;
  }
}


// ARROW FUNCTIONS
// Mendeklarasikan greet dengan menerima 2 paramter
const greet = (greeting = 'Hello', name = 'There') => `${greeting} ${name}`;
// Menjalankan greet
console.log(greet('Hi'));


// OOP

// Constructor Function
// Menjalankan program dengan 3 parameter
function Person(firstName, lastName, dob) {
  // Set object properties
  // Membuat object properties
  this.firstName = firstName;
  this.lastName = lastName;
  // Membuat set tanggal object
  this.dob = new Date(dob); // Set to actual date object using Date constructor
  // this.getBirthYear = function(){
  //   return this.dob.getFullYear();
  // }
  // this.getFullName = function() {
  //   return `${this.firstName} ${this.lastName}`
  // }
}

// Get Birth Year
// Membuat fungsi person dengan properti prototype pada variable getBirthYear
Person.prototype.getBirthYear = function () {
  // Mengembalikan value
  return this.dob.getFullYear();
}

// Get Full Name
// Membuat fungsi person dengan properti prototype pada variable getFullName
Person.prototype.getFullName = function() {
  // Mengembalikan value
  return `${this.firstName} ${this.lastName}`
}


// Instantiate an object from the class
// Mendeklarasikan dari variable 1 ke variable lainnya
const person1 = new Person('John', 'Doe', '7-8-80');
const person2 = new Person('Steve', 'Smith', '8-2-90');

// Menampilkan output variable person2
console.log(person2);

// console.log(person1.getBirthYear());
// console.log(person1.getFullName());



// Built in constructors
// Mendeklarasikan variable baru dari variable lain dengan value tertentu
const name = new String('Kevin');
// Menampilkan output object
console.log(typeof name); // Shows 'Object'
// Mendeklarasikan variable baru dari variable lain dengan value tertentu
const num = new Number(5);
// Menampilkan output object
console.log(typeof num); // Shows 'Object'


// ES6 CLASSES
// Membuat class Person
class Person {
  // Metode khusus yang akan dijalankan pertama kali pada suatu class / object
  constructor(firstName, lastName, dob) {
    // Membuat object property
    this.firstName = firstName;
    this.lastName = lastName;
    // Membuat set tanggal
    this.dob = new Date(dob);
  }

  // Get Birth Year
  // Menjalankan program getBirthYear
  getBirthYear() {
    // Mengembalikan value
    return this.dob.getFullYear();
  }

  // Get Full Name
  // Menjalankan program getFullName
  getFullName() {
    // Mengembalikan value
    return `${this.firstName} ${this.lastName}`
  }
}

// Mendeklarasikan variable baru dari variable yang lain dengan value const
const person1 = new Person('John', 'Doe', '7-8-80');
// Menampilkan output variabel person1 dan ditambah value getBirthYear
console.log(person1.getBirthYear());



// ELEMENT SELECTORS

// Mengambil objek elemen yang mewakili nilai css
console.log(document.getElementById('my-form'));
console.log(document.querySelector('.container'));
console.log(document.querySelectorAll('.item'));
console.log(document.getElementsByTagName('li'));
console.log(document.getElementsByClassName('item'));

// Mendeklarasikan variable dengan element selector
const items = document.querySelectorAll('.item');
// forEach untuk perulangan yang digunakan untuk mencetak item didalam array
items.forEach((item) => console.log(item));


// ul untuk membuat daftar tanpa nomor urut
const ul = document.querySelector('.items');
// ul.remove();
// ul.lastElementChild.remove();
// Mengembalikan elemen pertama atau null jika tidak ada elemen
ul.firstElementChild.textContent = 'Hello';
// Mendapatkan turunan dari elemen yang cocok yang difilter
ul.children[1].innerText = 'Brad';
// Mengembalikan elemen terakhir atau null jika tidak ada elemen
ul.lastElementChild.innerHTML = '<h1>Hello</h1>';

// Mendeklarasikan atribut button pada css
const btn = document.querySelector('.btn');
// btn.style.background = 'red';


// Memberikan kondisi btn di click
btn.addEventListener('click', e => {
  // Method untuk mencegah terjadinya event bawaan dari sebuah DOM
  e.preventDefault();
  // Mengambil object target
  console.log(e.target.className);
  // Memberikan background pada Id
  document.getElementById('my-form').style.background = '#ccc';
  //  Merubah warna background jadi gelap
  document.querySelector('body').classList.add('bg-dark');
  // Mengganti value terakhir atau null jika tidak ada elemen
  ul.lastElementChild.innerHTML = '<h1>Changed</h1>';
});

// Mendeklarasikan variable pada selector
const nameInput = document.querySelector('#name');
// Memberikan perintah untuk Listener elemen yang diinput
nameInput.addEventListener('input', e => {
  // append berfungsi untuk menambahkan elemen baru
  document.querySelector('.container').append(nameInput.value);
});


// Variable yang menampung value dari selector
const myForm = document.querySelector('#my-form');
const nameInput = document.querySelector('#name');
const emailInput = document.querySelector('#email');
const msg = document.querySelector('.msg');
const userList = document.querySelector('#users');

// Listen for form submit
// Melakukan listener pada element yang disubmit
myForm.addEventListener('submit', onSubmit);

// Menjalankan program onSubmit
function onSubmit(e) {
  // Method untuk menghentikan event default/bawaan
  e.preventDefault();
  
  // Memberikan kondisi ketika element input kosong
  if(nameInput.value === '' || emailInput.value === '') {
    // alert('Please enter all fields');
    // Menambah error messege
    msg.classList.add('error');
    // Menampilkan error messege pada user
    msg.innerHTML = 'Please enter all fields';

    // Remove error after 3 seconds
    // Menghilangkan error setelah 3 detik
    setTimeout(() => msg.remove(), 3000);
    // Memberikan kondisi lain
  } else {
    // Menyimpan text atau object yang sudah di klik submit
    const li = document.createElement('li');

    // Menampilkan text atau object yang sudah di submit
    li.appendChild(document.createTextNode(`${nameInput.value}: ${emailInput.value}`));

    // Add HTML
    // li.innerHTML = `<strong>${nameInput.value}</strong>e: ${emailInput.value}`;

    // Append to ul
    // Menambahkan element baru pada list item
    userList.appendChild(li);

    // Clear fields
    // Mengkosonkan bagian name dan email 
    nameInput.value = '';
    emailInput.value = '';
  }
}